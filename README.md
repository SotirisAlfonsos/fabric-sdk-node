## Argent on Hyperledger Fabric v1.0 Client SDK for Node.js

The Hyperledger Fabric Client SDK makes it easy to use APIs to interact with a Hyperledger Fabric blockchain. This repository is a fork of the official fabric-sdk-node v1.0.0-beta. We mainly worked with the balance-transfer example and introduced the new [Argent](https://bitbucket.org/SotirisAlfonsos/fabric-sdk-node/src/0029636117dd08a5336ee332350ee18cdb349b6c/test/fixtures/src/github.com/argent_example_cc/argent_cc.go?at=master&fileviewer=file-view-default) smart contract.

All our work and tests has been carried out with the v1.0.0-beta version.

### Build and Test
To build and test, the following prerequisites must be installed first:

* node runtime version 6.9.x, note that 7.0 is not supported at this point
* npm tool version 3.10.x
* gulp command (must be installed globally with `npm install -g gulp`)
* docker (not required if you only want to run the headless tests with `npm test`, see below)

# Build fabric:
`cd $GOPATH/src/github.com/hyperledger/`   
`git clone https://github.com/hyperledger/fabric`   
`cd $GOPATH/src/github.com/hyperledger/fabric/`   
`git checkout v1.0.0-beta`   
`make docker`   
  
# Build fabric-ca:    
`cd $GOPATH/src/github.com/hyperledger/`     
`git clone https://github.com/hyperledger/fabric-ca`     
`cd $GOPATH/src/github.com/hyperledger/fabric-ca/`     
`git checkout v1.0.0-beta`   
`make docker`   
  
# Build fabric-sdk-node:
`cd $GOPATH/src/github.com/hyperledger/`  
`git clone https://bitbucket.org/SotirisAlfonsos/fabric-sdk-node`   
`cd $GOPATH/src/github.com/hyperledger/fabric-sdk-node/`

Clone the project and launch the following commands to install the dependencies and perform various tasks.

In the project root folder:
* `npm install` to install dependencies
* optionally, `gulp watch` to set up watch that updates fabric-ca-client's shared dependencies from fabric-client/lib and updates installed fabric-client and fabric-ca-client modules in node_modules. This command does not return, so you should keep it running in a separate command window as you work on the code and test in another command window. Note that you do NOT need to run this unless you plan to make changes in the fabric-client and fabric-ca-client packages

### Testing Argent
There are two ways to work with Argent. The first is through a [REST API](https://bitbucket.org/SotirisAlfonsos/fabric-sdk-node/src/0029636117dd/examples/balance-transfer/?at=master) and the second one in [myE2eExample](https://bitbucket.org/SotirisAlfonsos/fabric-sdk-node/src/0029636117dd08a5336ee332350ee18cdb349b6c/examples/balance-transfer/myE2eExample/?at=master) does a full run ( create channel, enroll users, join channel, install chaincode, instantiate chaincode, invoke chaincode, query chaincode )

More details in the `/examples/balance-transfer` folder.
