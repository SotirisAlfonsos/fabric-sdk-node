#!/bin/bash

jq --version > /dev/null 2>&1
if [ $? -ne 0 ]; then
        echo "Please Install 'jq' https://stedolan.github.io/jq/ to execute this script"
        echo
        exit 1
fi
starttime=$(date +%s)
#=====================INIT ORGS=======================
echo "POST request Enroll on Org1  ..."
echo
ORG1_TOKEN=$(curl -s -X POST \
  http://localhost:4000/users \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=ING&orgName=org1')
echo $ORG1_TOKEN
ORG1_TOKEN=$(echo $ORG1_TOKEN | jq ".token" | sed "s/\"//g")
echo
echo "ORG1 token is $ORG1_TOKEN"
echo
echo "POST request Enroll on Org2 ..."
echo
ORG2_TOKEN=$(curl -s -X POST \
  http://localhost:4000/users \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=ABN&orgName=org2')
echo $ORG2_TOKEN
ORG2_TOKEN=$(echo $ORG2_TOKEN | jq ".token" | sed "s/\"//g")
echo
echo "ORG2 token is $ORG2_TOKEN"

echo "POST request Enroll on Org3 ..."
echo
ORG3_TOKEN=$(curl -s -X POST \
  http://localhost:4001/users \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=Rabo&orgName=org3')
echo $ORG3_TOKEN
ORG3_TOKEN=$(echo $ORG3_TOKEN | jq ".token" | sed "s/\"//g")
echo
echo "ORG3 token is $ORG3_TOKEN"
#=====================INIT ORGS=======================


#=====================CREATE CHANNELS=======================
echo "POST request Create channel  ..."
echo
curl -s -X POST \
  http://localhost:4000/channels \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "channelName":"mychannel",
        "channelConfigPath":"../artifacts/channel/mychannel.tx"
}'
echo
echo
sleep 5

echo "POST request Create channel  ..."
echo
curl -s -X POST \
  http://localhost:4001/channels \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "channelName":"mychannel2",
        "channelConfigPath":"../artifacts/channel/mychannel2.tx"
}'
echo
echo
sleep 5
#=====================CREATE CHANNELS=======================



#=====================PEERS JOIN CHANNELS=======================
echo "POST request Join channel on Org1"
echo
curl -s -X POST \
  http://localhost:4000/channels/mychannel/peers \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG1':7051","'$PEER1_ORG1':7056"]
}'
echo
echo

echo "POST request Join channel on Org1"
echo
curl -s -X POST \
  http://localhost:4001/channels/mychannel2/peers \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG1':7051","'$PEER1_ORG1':7056"]
}'
echo
echo

echo "POST request Join channel on Org2"
echo
curl -s -X POST \
  http://localhost:4000/channels/mychannel/peers \
  -H "authorization: Bearer $ORG2_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG2':8051","'$PEER1_ORG2':8056"]
}'
echo
echo

echo "POST request Join channel on Org3"
echo
curl -s -X POST \
  http://localhost:4001/channels/mychannel2/peers \
  -H "authorization: Bearer $ORG3_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG3':8151","'$PEER1_ORG3':8156"]
}'
echo
echo
#=====================PEERS JOIN CHANNELS=======================



#=====================INSTALL CHAINCODE ON PEERS=======================
echo "POST Install chaincode on Org1 channel 1"
echo
curl -s -X POST \
  http://localhost:4000/chaincodes \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER1_ORG1':7056"],
        "chaincodeName":"mycc",
        "chaincodePath":"github.com/argent_example_cc",
        "chaincodeVersion":"v0"
}'
echo
echo

echo "POST Install chaincode on Org1 channel 1"
echo
curl -s -X POST \
  http://localhost:4000/chaincodes \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG1':7051"],
        "chaincodeName":"mycc",
        "chaincodePath":"github.com/argent_example_cc",
        "chaincodeVersion":"v0"
}'
echo
echo

echo "POST Install chaincode on Org2 channel 1"
echo
curl -s -X POST \
  http://localhost:4000/chaincodes \
  -H "authorization: Bearer $ORG2_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG2':8051","'$PEER1_ORG2':8056"],
        "chaincodeName":"mycc",
        "chaincodePath":"github.com/argent_example_cc",
        "chaincodeVersion":"v0"
}'
echo
echo

echo "POST Install chaincode on Org1 channel 2"
echo
curl -s -X POST \
  http://localhost:4001/chaincodes \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG1':7051","'$PEER1_ORG1':7056"],
        "chaincodeName":"mycc2",
        "chaincodePath":"github.com/argent_example_cc_ch2",
        "chaincodeVersion":"v0"
}'
echo
echo

echo "POST Install chaincode on Org3 channel 2"
echo
curl -s -X POST \
  http://localhost:4001/chaincodes \
  -H "authorization: Bearer $ORG3_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG3':8151","'$PEER1_ORG3':8156"],
        "chaincodeName":"mycc2",
        "chaincodePath":"github.com/argent_example_cc_ch2",
        "chaincodeVersion":"v0"
}'
echo
echo
#=====================INSTALL CHAINCODE ON PEERS=======================



#=====================INSTANTIATE CHAINCODE ON PEERS=======================
echo "POST instantiate chaincode on peer1 of Org1"
echo
curl -s -X POST \
  http://localhost:4000/channels/mychannel/chaincodes \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "chaincodeName":"mycc",
        "chaincodeVersion":"v0",
        "functionName":"init",
        "args":["Euro","0","200","-200","300","-300","0","0"]
}'

echo "POST instantiate chaincode on peer1 of Org1"
echo
curl -s -X POST \
  http://localhost:4001/channels/mychannel2/chaincodes \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "chaincodeName":"mycc2",
        "chaincodeVersion":"v0",
        "functionName":"init",
        "args":["Euro","0","200","-200","300","-300","30","30"]
}'

#[currency, ammount, first limit or1, second limit or1, first limit or2, second limit or2, running, posted]
echo
echo
#=====================INSTALL CHAINCODE ON PEERS=======================



echo "Total execution time : $(($(date +%s)-starttime)) secs ..."

