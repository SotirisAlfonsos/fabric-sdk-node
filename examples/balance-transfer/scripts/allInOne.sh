#!/bin/bash

count=1

echo "POST request Enroll on Org1  ..."
echo
ORG1_TOKEN=$(curl -s -X POST \
    http://localhost:4000/users \
   -H "cache-control: no-cache" \
   -H "content-type: application/x-www-form-urlencoded" \
   -d 'username=ING&orgName=org1')
echo $ORG1_TOKEN
ORG1_TOKEN=$(echo $ORG1_TOKEN | jq ".token" | sed "s/\"//g")
echo

echo "POST request Enroll on Org2  ..."
echo
ORG2_TOKEN=$(curl -s -X POST \
  http://localhost:4000/users \
  -H "cache-control: no-cache" \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=ABN&orgName=org2')
echo $ORG2_TOKEN
ORG2_TOKEN=$(echo $ORG2_TOKEN | jq ".token" | sed "s/\"//g")
echo

echo "POST request Enroll on Org3  ..."
echo
ORG3_TOKEN=$(curl -s -X POST \
  http://localhost:4001/users \
  -H "cache-control: no-cache" \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=Rabo3&orgName=org3')
echo $ORG3_TOKEN
ORG3_TOKEN=$(echo $ORG3_TOKEN | jq ".token" | sed "s/\"//g")
echo

sleep 20
starttime=$(date +%s)

while [ $(($(date +%s)-starttime)) -le 1 ]
do
	#bash limitINGChange.sh & 
	bash newABNtransactionStorm.sh "$ORG2_TOKEN" &
	bash newINGtransactionStorm.sh "$ORG1_TOKEN" &
	bash newRabotransactionStorm.sh "$ORG3_TOKEN" &
	#bash newABNtransaction.sh $((count+2)) "$ORG2_TOKEN" &
        #bash newINGtransaction.sh $((count+3)) "$ORG1_TOKEN" &
	count=$((count+4))

#	docker restart peer1 &
#docker stop peer1 &
#docker start peer1 &
	#bash queryState.sh &
	#bash queryAll.sh &
done
echo "=============$count================"
