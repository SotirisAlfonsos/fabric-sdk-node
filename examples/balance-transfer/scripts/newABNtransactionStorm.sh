#!/bin/bash

ORG2_TOKEN="$1"

echo "POST invoke chaincode on peers of Org2"
echo
TRX_ID=$(curl -s -X POST \
  http://localhost:4000/channels/mychannel/chaincodes/mycc \
  -H "authorization: Bearer $ORG2_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG1':7051","'$PEER1_ORG1':7056","'$PEER0_ORG2':8051","'$PEER1_ORG2':8056"],
        "fcn":"invoke",
        "args":["transactionProposal","Euro","20"]
}')
echo $TRX_ID
echo

