#!/bin/bash

#TX_ID="$1"
#ORG1_TOKEN="$2"

echo "POST request Enroll on Org1  ..."
echo
ORG1_TOKEN=$(curl -s -X POST \
  http://localhost:4000/users \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=ING&orgName=org1')
echo $ORG1_TOKEN
ORG1_TOKEN=$(echo $ORG1_TOKEN | jq ".token" | sed "s/\"//g")
echo

echo "POST invoke chaincode on peers of Org1"
echo
TRX_ID=$(curl -s -X POST \
  http://localhost:4001/channels/mychannel2/chaincodes/mycc2 \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG1':7051","'$PEER1_ORG1':7056","'$PEER0_ORG3':8151","'$PEER1_ORG3':8156"],
        "fcn":"invoke",
        "args":["transactionProposal","Euro","20"]
}')
echo "Transacton ID is $TRX_ID"
echo
echo

#TX_ID=$((TX_ID+1))

#echo "POST invoke chaincode on peers of Org1"
#echo
#echo "org1 token is $ORG1_TOKEN"
#TRX_ID=$(curl -s -X POST \
#  http://localhost:4000/channels/mychannel/chaincodes/mycc \
#  -H "authorization: Bearer $ORG1_TOKEN" \
#  -H "cache-control: no-cache" \
#  -H "content-type: application/json" \
#  -H "x-access-token: $ORG1_TOKEN" \
#  -d '{
#        "peers": ["'$PEER0_ORG1':7051"],
#        "chaincodeVersion":"v0",
#        "functionName":"invoke",
#        "args":["transactionProposal","Euro","20","'$TX_ID'"]
#}')
#echo "Transacton ID is $TRX_ID"
#echo
#echo

