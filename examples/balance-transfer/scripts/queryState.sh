#!/bin/bash

echo "POST request Enroll on Org3 ..."
echo
ORG_TOKEN=$(curl -s -X POST \
  http://localhost:4001/users \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=Rabo3&orgName=org3')
echo $ORG_TOKEN
ORG_TOKEN=$(echo $ORG_TOKEN | jq ".token" | sed "s/\"//g")
echo

echo "GET query chaincode on peer2 of Org3 on channel 2"
echo
curl -s -X GET \
  "http://localhost:4001/channels/mychannel2/chaincodes/mycc2?peer=peer2&args=%5B%22queryBase%22%5D" \
  -H "authorization: Bearer $ORG_TOKEN" \
  -H "content-type: application/json" \
echo
echo

echo "POST request Enroll on Org2 ..."
echo
ORG_TOKEN=$(curl -s -X POST \
  http://localhost:4000/users \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=ABN3&orgName=org2')
echo $ORG_TOKEN
ORG_TOKEN=$(echo $ORG_TOKEN | jq ".token" | sed "s/\"//g")
echo

echo "GET query chaincode on peer2 of Org2 on channel 1"
echo
curl -s -X GET \
  "http://localhost:4000/channels/mychannel/chaincodes/mycc?peer=peer2&args=%5B%22queryBase%22%5D" \
  -H "authorization: Bearer $ORG_TOKEN" \
  -H "content-type: application/json" \
echo
echo

