#!/bin/bash

ORG1_TOKEN="$1"

echo "POST invoke chaincode on from org1 on channel 2"
echo
TRX_ID=$(curl -s -X POST \
  http://localhost:4001/channels/mychannel2/chaincodes/mycc2 \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG1':7051","'$PEER1_ORG1':7056","'$PEER0_ORG3':8151","'$PEER1_ORG3':8156"],
        "fcn":"invoke",
        "args":["transactionProposal","Euro","20"]
}')
echo "Transacton ID is $TRX_ID"
echo
echo

echo "POST invoke chaincode on from org1 on channel 1"
echo
TRX_ID=$(curl -s -X POST \
  http://localhost:4000/channels/mychannel/chaincodes/mycc \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG1':7051","'$PEER1_ORG1':7056","'$PEER0_ORG3':8151","'$PEER1_ORG3':8156"],
        "fcn":"invoke",
        "args":["transactionProposal","Euro","20"]
}')
echo "Transacton ID is $TRX_ID"
echo
echo

