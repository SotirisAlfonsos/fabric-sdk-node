#!/bin/bash

#sudo iptables -L -n -v
#tc -s qdisc show dev vetha66f6e4

#   to find the docker veth corelation.

#BRIDGE_NAME="br-9fe125a4a0bf"
DELAY_UP_DOWN="$1"

function init() {

	number=$(docker exec -it orderer.example.com cat /sys/class/net/eth0/iflink)
        newNum=$(echo "$number+1" | tr -d $'\r' | bc)
        str=$( ip ad | grep "$newNum" )
        temp=$(echo $str | cut -f1 -d:)
        ord=$(echo $str | cut -f2 -d:)
	
	number=$(docker exec -it peer0.org1.example.com cat /sys/class/net/eth0/iflink)
	newNum=$(echo "$number+1" | tr -d $'\r' | bc)
	str=$( ip ad | grep "$newNum" )
	temp=$(echo $str | cut -f1 -d:)
	veth1=$(echo $str | cut -f2 -d:)

	number=$(docker exec -it peer1.org1.example.com cat /sys/class/net/eth0/iflink)
	newNum=$(echo "$number+1" | tr -d $'\r' | bc)
	str=$( ip ad | grep "$newNum" )
	temp=$(echo $str | cut -f1 -d:)
	veth2=$(echo $str | cut -f2 -d:)

	number=$(docker exec -it peer0.org2.example.com cat /sys/class/net/eth0/iflink)
        newNum=$(echo "$number+1" | tr -d $'\r' | bc)
        str=$( ip ad | grep "$newNum" )
        temp=$(echo $str | cut -f1 -d:)
        veth3=$(echo $str | cut -f2 -d:)

        number=$(docker exec -it peer1.org2.example.com cat /sys/class/net/eth0/iflink)
        newNum=$(echo "$number+1" | tr -d $'\r' | bc)
        str=$( ip ad | grep "$newNum" )
        temp=$(echo $str | cut -f1 -d:)
        veth4=$(echo $str | cut -f2 -d:)

	number=$(docker exec -it peer0.org3.example.com cat /sys/class/net/eth0/iflink)
        newNum=$(echo "$number+1" | tr -d $'\r' | bc)
        str=$( ip ad | grep "$newNum" )
        temp=$(echo $str | cut -f1 -d:)
        veth5=$(echo $str | cut -f2 -d:)

        number=$(docker exec -it peer1.org3.example.com cat /sys/class/net/eth0/iflink)
        newNum=$(echo "$number+1" | tr -d $'\r' | bc)
        str=$( ip ad | grep "$newNum" )
        temp=$(echo $str | cut -f1 -d:)
        veth6=$(echo $str | cut -f2 -d:)


	echo "$ord"
	echo "$veth1"
	echo "$veth2"
	echo "$veth3"
        echo "$veth4"
	echo "$veth5"
        echo "$veth6"
	#$(sudo tc qdisc add dev ${veth1} root netem loss 100%)
	#echo "You did not apply delays. Just seeing the veths" 
}

function create() {
	init
	#Iptables to mark packets from orderer -> Org1 and from Org1 -> orderer
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.12 -d 172.24.0.13 -j CLASSIFY --set-class 1:10)
	$(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.12 -d 172.24.0.13 -j RETURN)
	$(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.12 -d 172.24.0.14 -j CLASSIFY --set-class 1:11)
	$(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.12 -d 172.24.0.14 -j RETURN)
	$(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.13 -d 172.24.0.12 -j CLASSIFY --set-class 1:12)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.13 -d 172.24.0.12 -j RETURN)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.14 -d 172.24.0.12 -j CLASSIFY --set-class 1:12)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.14 -d 172.24.0.12 -j RETURN)

	#Iptables to mark packets from orderer -> Org2 and from Org2 -> orderer
	$(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.12 -d 172.24.0.15 -j CLASSIFY --set-class 1:14)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.12 -d 172.24.0.15 -j RETURN)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.12 -d 172.24.0.16 -j CLASSIFY --set-class 1:15)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.12 -d 172.24.0.16 -j RETURN)
	$(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.15 -d 172.24.0.12 -j CLASSIFY --set-class 1:16)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.15 -d 172.24.0.12 -j RETURN)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.16 -d 172.24.0.12 -j CLASSIFY --set-class 1:16)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.16 -d 172.24.0.12 -j RETURN)

	#Iptables to mark packets from orderer -> Org3 and from Org3 -> orderer
	$(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.12 -d 172.24.0.17 -j CLASSIFY --set-class 1:18)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.12 -d 172.24.0.17 -j RETURN)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.12 -d 172.24.0.18 -j CLASSIFY --set-class 1:19)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.12 -d 172.24.0.18 -j RETURN)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.17 -d 172.24.0.12 -j CLASSIFY --set-class 1:20)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.17 -d 172.24.0.12 -j RETURN)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.18 -d 172.24.0.12 -j CLASSIFY --set-class 1:20)
        $(sudo iptables -t mangle -A POSTROUTING -s 172.24.0.18 -d 172.24.0.12 -j RETURN)


	#Tc rules to delay the marked packets on peers
	$(sudo tc qdisc add dev ${veth1} root handle 1: htb)
	$(sudo tc class add dev ${veth1} parent 1:1 classid 1:10 htb rate 100mbit)
	$(sudo tc qdisc add dev ${veth1} parent 1:10 handle 100:0 netem delay 15ms 5ms 25%)

        $(sudo tc qdisc add dev ${veth2} root handle 1: htb)
        $(sudo tc class add dev ${veth2} parent 1:1 classid 1:11 htb rate 100mbit)
        $(sudo tc qdisc add dev ${veth2} parent 1:11 handle 110:0 netem delay 15ms 5ms 25%)

	$(sudo tc qdisc add dev ${veth3} root handle 1: htb)
        $(sudo tc class add dev ${veth3} parent 1:1 classid 1:14 htb rate 100mbit)
        $(sudo tc qdisc add dev ${veth3} parent 1:14 handle 140:0 netem delay 170ms 30ms 25% loss 2%)

	$(sudo tc qdisc add dev ${veth4} root handle 1: htb)
        $(sudo tc class add dev ${veth4} parent 1:1 classid 1:15 htb rate 100mbit)
        $(sudo tc qdisc add dev ${veth4} parent 1:15 handle 150:0 netem delay 170ms 30ms 25% loss 2%)

	$(sudo tc qdisc add dev ${veth5} root handle 1: htb)
        $(sudo tc class add dev ${veth5} parent 1:1 classid 1:18 htb rate 100mbit)
        $(sudo tc qdisc add dev ${veth5} parent 1:18 handle 180:0 netem delay 350ms 50ms 25% loss 5%)

	$(sudo tc qdisc add dev ${veth6} root handle 1: htb)
        $(sudo tc class add dev ${veth6} parent 1:1 classid 1:19 htb rate 100mbit)
        $(sudo tc qdisc add dev ${veth6} parent 1:19 handle 190:0 netem delay 350ms 50ms 25% loss 5%)

	$(sudo tc qdisc add dev ${ord} root handle 1: htb)
        $(sudo tc class add dev ${ord} parent 1:1 classid 1:12 htb rate 30mbit)
        $(sudo tc class add dev ${ord} parent 1:1 classid 1:16 htb rate 30mbit)
        $(sudo tc class add dev ${ord} parent 1:1 classid 1:20 htb rate 30mbit)
        $(sudo tc qdisc add dev ${ord} parent 1:12 handle 120:0 netem delay 15ms 5ms 25%)
        $(sudo tc qdisc add dev ${ord} parent 1:16 handle 160:0 netem delay 170ms 30ms 25% loss 2%)
        $(sudo tc qdisc add dev ${ord} parent 1:20 handle 200:0 netem delay 350ms 50ms 25% loss 5%)

#       $(sudo iptables -I FORWARD 1 -d 172.24.0.13 -s 172.24.0.12 -j ORD -m comment --comment "Drop connection from the orderer(Netherlands) to peer0 Org1(China)")
#       $(sudo iptables -I FORWARD 1 -d 172.24.0.14 -s 172.24.0.12 -j DROP -m comment --comment "Drop connection from the orderer(Netherlands) to peer1 Org1(China)")

#	"$(sudo iptables -L -n -v)"
}

function stop() {
	init

#	$(sudo iptables -t mangle -D FORWARD -d 172.24.0.13 -s 172.24.0.12 -j CLASSIFY --set-class 1:2 -m comment --comment "Drop connection from the orderer(Netherlands) to peer0 Org1(China)")
#        $(sudo iptables -t mangle -D FORWARD -d 172.24.0.14 -s 172.24.0.12 -j CLASSIFY --set-class 2:2 -m comment --comment "Drop connection from the orderer(Netherlands) to peer1 Org1(China)")
#	$(sudo iptables -D FORWARD -d 172.24.0.13 -s 172.24.0.12 -j PREROUTING -m comment --comment "Drop connection from the orderer(Netherlands) to peer0 Org1(China)")
#       $(sudo iptables -D FORWARD -d 172.24.0.14 -s 172.24.0.12 -j PREROUTING -m comment --comment "Drop connection from the orderer(Netherlands) to peer1 Org1(China)")
#        $(sudo iptables -t nat -D PREROUTING -m mark --mark 11 -j DNAT --to-destination 172.24.0.14)
#	$(sudo iptables -t mangle -D PREROUTING -s 172.24.0.12 -d 172.24.0.13 -j MARK --set-mark 11)
#	$(sudo iptables -t nat -D POSTROUTING -s 192.168.100.199 -j SNAT --to-source 172.24.0.13)
#	$(sudo iptables -t mangle -D PREROUTING -s 172.24.0.12 -d 172.24.0.13 -j MARK --set-mark 11)
#        $(sudo iptables -t mangle -D PREROUTING -s 172.24.0.12 -d 172.24.0.13 -j MARK --set-mark 11)
#        $(sudo iptables -t mangle -D PREROUTING -s 172.24.0.12 -d 172.24.0.15 -j MARK --set-mark 12)
	$(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.12 -d 172.24.0.13 -j CLASSIFY --set-class 1:10)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.12 -d 172.24.0.13 -j RETURN)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.12 -d 172.24.0.14 -j CLASSIFY --set-class 1:11)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.12 -d 172.24.0.14 -j RETURN)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.13 -d 172.24.0.12 -j CLASSIFY --set-class 1:12)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.13 -d 172.24.0.12 -j RETURN)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.14 -d 172.24.0.12 -j CLASSIFY --set-class 1:12)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.14 -d 172.24.0.12 -j RETURN)

        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.12 -d 172.24.0.15 -j CLASSIFY --set-class 1:14)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.12 -d 172.24.0.15 -j RETURN)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.12 -d 172.24.0.16 -j CLASSIFY --set-class 1:15)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.12 -d 172.24.0.16 -j RETURN)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.15 -d 172.24.0.12 -j CLASSIFY --set-class 1:16)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.15 -d 172.24.0.12 -j RETURN)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.16 -d 172.24.0.12 -j CLASSIFY --set-class 1:16)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.16 -d 172.24.0.12 -j RETURN)

        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.12 -d 172.24.0.17 -j CLASSIFY --set-class 1:18)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.12 -d 172.24.0.17 -j RETURN)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.12 -d 172.24.0.18 -j CLASSIFY --set-class 1:19)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.12 -d 172.24.0.18 -j RETURN)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.17 -d 172.24.0.12 -j CLASSIFY --set-class 1:20)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.17 -d 172.24.0.12 -j RETURN)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.18 -d 172.24.0.12 -j CLASSIFY --set-class 1:20)
        $(sudo iptables -t mangle -D POSTROUTING -s 172.24.0.18 -d 172.24.0.12 -j RETURN)


	$(sudo tc qdisc del dev ${veth1} root)
        $(sudo tc qdisc del dev ${veth2} root)
        $(sudo tc qdisc del dev ${veth3} root)
        $(sudo tc qdisc del dev ${veth4} root)
        $(sudo tc qdisc del dev ${veth5} root)
        $(sudo tc qdisc del dev ${veth6} root)
	$(sudo tc qdisc del dev ${ord} root)
#	$(sudo iptables -D FORWARD -d 172.24.0.13 -s 172.24.0.12 -j DROP -m comment --comment "Drop connection from the orderer to peer0 Org1")
#       $(sudo iptables -D FORWARD -d 172.24.0.14 -s 172.24.0.12 -j DROP -m comment --comment "Drop connection from the orderer to peer1 Org1")

	#$(sudo ip addr del 192.168.100.199/24 brd + dev testveth)
	#$(sudo ip link delete testveth type dummy)
	#$(sudo rmmod dummy)

#       "$(sudo iptables -L -n -v)"
}

if [ "${DELAY_UP_DOWN}" == "up" ]; then
        create
	echo "Here is some network delay for you"
elif [ "${DELAY_UP_DOWN}" == "down" ]; then ## Clear the network
        stop
	echo "All custom network delays have been removed. Hopefully"
else
        init
        exit 1
fi

