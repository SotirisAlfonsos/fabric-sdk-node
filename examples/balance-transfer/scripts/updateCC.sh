#!/bin/bash

echo "POST request token on Org2 channel 1 ..."
echo
ORG2_TOKEN=$(curl -s -X POST \
  http://localhost:4000/users \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=ABN&orgName=org2')
echo $ORG2_TOKEN
ORG2_TOKEN=$(echo $ORG2_TOKEN | jq ".token" | sed "s/\"//g")
echo

echo "POST request token on Org1 channel 2  ..."
echo
ORG1_TOKEN=$(curl -s -X POST \
  http://localhost:4001/users \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=ING&orgName=org1')
echo $ORG1_TOKEN
ORG1_TOKEN=$(echo $ORG1_TOKEN | jq ".token" | sed "s/\"//g")
echo

echo "POST request token on Org3 channel 2  ..."
echo
ORG3_TOKEN=$(curl -s -X POST \
  http://localhost:4001/users \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=Rabo2&orgName=org3')
echo $ORG3_TOKEN
ORG3_TOKEN=$(echo $ORG3_TOKEN | jq ".token" | sed "s/\"//g")
echo

echo "POST Install chaincode update on Org2 Org1 channel 1"
echo
curl -s -X POST \
  http://localhost:4000/chaincodes \
  -H "authorization: Bearer $ORG2_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG2':8051","'$PEER1_ORG2':8056"],
        "chaincodeName":"mycc",
        "chaincodePath":"github.com/argent_example_cc",
        "chaincodeVersion":"v2"
}'
echo
echo

echo "POST Install chaincode update on Org1 channel 2"
echo
curl -s -X POST \
  http://localhost:4001/chaincodes \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG1':7051","'$PEER1_ORG1':7056"],
        "chaincodeName":"mycc2",
        "chaincodePath":"github.com/argent_example_cc_ch2",
        "chaincodeVersion":"v2"
}'
echo
echo

echo "POST Install chaincode update on Org3 channel 2"
echo
curl -s -X POST \
  http://localhost:4001/chaincodes \
  -H "authorization: Bearer $ORG3_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG3':8151","'$PEER1_ORG3':8156"],
        "chaincodeName":"mycc2",
        "chaincodePath":"github.com/argent_example_cc_ch2",
        "chaincodeVersion":"v2"
}'
echo
echo

echo "POST Install chaincode update on Org1 channel 1"
echo
curl -s -X POST \
  http://localhost:4000/chaincodes \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG1':7051","'$PEER1_ORG1':7056"],
        "chaincodeName":"mycc",
        "chaincodePath":"github.com/argent_example_cc",
        "chaincodeVersion":"v2"
}'
echo
echo

