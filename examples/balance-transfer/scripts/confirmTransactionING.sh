#!/bin/bash

TXNUMBER="$1"

echo "POST request Enroll on Org1  ..."
echo
ORG1_TOKEN=$(curl -s -X POST \
  http://localhost:4000/users \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=ING&orgName=org1')
echo $ORG1_TOKEN
ORG1_TOKEN=$(echo $ORG1_TOKEN | jq ".token" | sed "s/\"//g")
echo

echo "POST invoke chaincode on peers of Org1"
echo
echo "org1 token is $ORG1_TOKEN"
TRX_ID=$(curl -s -X POST \
  http://localhost:4000/channels/mychannel/chaincodes/mycc \
  -H "authorization: Bearer $ORG1_TOKEN" \
  -H "content-type: application/json" \
  -d '{
        "peers": ["'$PEER0_ORG1':7051", "'$PEER1_ORG1':7056"],
        "fcn":"invoke",
        "args":["transactionConfirmation","'${TXNUMBER}'"]
}')
echo "Transacton ID is $TRX_ID"
echo

