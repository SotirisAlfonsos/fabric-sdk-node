## Balance transfer

A sample Node.js app to demonstrate **__fabric-client__** & **__fabric-ca-client__** Node.js SDK APIs

### Prerequisites and setup: 

* [Docker](https://www.docker.com/products/overview) - v1.12 or higher
* [Docker Compose](https://docs.docker.com/compose/overview/) - v1.8 or higher
* [Git client](https://git-scm.com/downloads) - needed for clone commands
* **Node.js** v6.2.0 - 6.10.0 ( __Node v7+ is not supported__ )
* Download docker images

```
cd fabric-sdk-node/examples/balance-transfer/
./runApp.sh
```

Once you have completed the above setup, you will be provisioned a local network with following configuration:

* 3 CAs
* A kafka ordering service with 4 brokers, 3 zookeepers and one orderer
* 4 peers (2 peers per Org) on channel 1   
* 4 peers (2 peers per Org) on channel 2
* 3 Orgs in total

#### Artifacts
* Crypto material has been generated using the **cryptogen** tool from fabric and mounted to all peers, the orderering node and CA containers. More details regarding the cryptogen tool are available [here](http://hyperledger-fabric.readthedocs.io/en/latest/getting_started.html#using-the-cryptogen-tool).
* An Orderer genesis block (genesis.block) and channel configuration transactions (mychannel.tx, mychannel2.tx) have been pre generated using the **configtxgen** tool and placed within the artifacts folder. More details regarding the configtxgen tool are available [here](http://hyperledger-fabric.readthedocs.io/en/latest/getting_started.html#using-the-configtxgen-tool).

## Running the sample program

There are two options available for running the Argent use case

### Option 1:

##### REST API

Before you start it is important to fix the IPs for the docker containers. You should do that in the following files:

* `artifacts/docker-compose.yaml`
* `config.json` and `config2.json`
* `app/network-config.json` and `app2/network-config2.json`
* `scripts/.env`

Running the experiments:

* Launch the network using `./runApp.sh`
* On separate terminal run `PORT=4000 node app.js`
* On separate terminal run `PORT=4001 node app2.js`
* Those two commands will create two running APIs for each of our two channels
* **Optional.** Running the `testAPIs.sh` we perform simple API request. This is only used to test that everything is setup correctly.
* Go into the `scripts` folder. Running `initArgent.sh` will initialise our network and we can start performing requests ( invoke ).
* Use the different scripts to change the limits, propose transactions, confirm transactions, or query for values. The `networkDelay.sh` script applies network delays to some connections but it is not finished yet.

### Option 2:

##### myE2eExample

Before you start it is important to fix the IPs for the docker containers. You should do that in the following files:

* `artifacts/docker-compose.yaml`
* `myE2eExample/config.json` and `myE2eExample/config2.json`
* `myE2eExample/app/network-config.json` and `myE2eExample/app2/network-config2.json`
* `myE2eExample/sampleRequests.json`

Running the experiments:

* Launch the network using `./runApp.sh`
* `cd myE2eExample`
* run `node run.js`

This will perform a full network setup and will finish with querying the final values at the end.

