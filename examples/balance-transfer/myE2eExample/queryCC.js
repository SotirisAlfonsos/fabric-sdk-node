'use strict';
var log4js = require('log4js');
var logger = log4js.getLogger('My application');
logger.setLevel('DEBUG');

var query = require('./app/query.js');

var samples = require('./sampleRequests.json');

function defaultRun() {

    var org1ch1 = QueryCC(samples.allInOne_Org1_Ch1, samples.sampleQueryFunction0);
    var org1ch2 = QueryCC(samples.allInOne_Org1_Ch2, samples.sampleQueryFunction1);
    var org1ch1 = QueryCC(samples.allInOne_Org1_Ch1, samples.sampleQueryAllFunction0);
    var org1ch2 = QueryCC(samples.allInOne_Org1_Ch2, samples.sampleQueryAllFunction1);

    return Promise.all( [ org1ch1, org1ch2, org1ch1, org1ch2 ] ).then(message => {
        logger.debug('==========Smart contract query is successful=========');
        return 'Smart contract query is successful';
    }).catch((err) => {
        logger.debug('========Query on smart contract failed=======');
        return err;
    });

}

// Query on chaincode on target peers
function QueryCC(req, queryArgs) {
	logger.debug('==================== QUERY BY CHAINCODE ==================');
	var channelName = req.channelName;
	var chaincodeName = req.chaincodeName;
	let args = queryArgs.args;
	let peer = queryArgs.peer;

	logger.debug('channelName : ' + channelName);
	logger.debug('chaincodeName : ' + chaincodeName);
	logger.debug('args : ' + args);

	if (!chaincodeName) {
		return 'No smart contract name';
	}

	if (!channelName) {
		return 'No channel name';
	}

	if (!args) {
		return 'No args';
	}

	return query.queryChaincode(peer, channelName, chaincodeName, args, req.userName, req.orgName)
	.then(function(message) {
		return message;
	}).catch((err) => {
        logger.debug('========Query failure=======');
        logger.debug(err);
        throw err;
    });
}

/************************************************************
We have not configured those queries yet!
************************************************************/

//  Query Get Block by BlockNumber
function QueryBlockName(req, res) {
	logger.debug('==================== GET BLOCK BY NUMBER ==================');
	let blockId = req.params.blockId;
	let peer = req.query.peer;
	logger.debug('channelName : ' + req.params.channelName);
	logger.debug('BlockID : ' + blockId);
	logger.debug('Peer : ' + peer);
	if (!blockId) {
		res.json(getErrorMessage('\'blockId\''));
		return;
	}

	query.getBlockByNumber(peer, blockId, req.username, req.orgname)
		.then(function(message) {
			return message;
		});
}
// Query Get Transaction by Transaction ID
function QueryTxId(req, res) {
	logger.debug(
		'================ GET TRANSACTION BY TRANSACTION_ID ======================'
	);
	logger.debug('channelName : ' + req.params.channelName);
	let trxnId = req.params.trxnId;
	let peer = req.query.peer;
	if (!trxnId) {
		res.json(getErrorMessage('\'trxnId\''));
		return;
	}

	query.getTransactionByID(peer, trxnId, req.username, req.orgname)
		.then(function(message) {
			return message;
		});
}
// Query Get Block by Hash
function QueryBlockHash(req, res) {
	logger.debug('================ GET BLOCK BY HASH ======================');
	logger.debug('channelName : ' + req.params.channelName);
	let hash = req.query.hash;
	let peer = req.query.peer;
	if (!hash) {
		res.json(getErrorMessage('\'hash\''));
		return;
	}

	query.getBlockByHash(peer, hash, req.username, req.orgname).then(
		function(message) {
			return message;
		});
}
//Query for Channel Information
function QueryChannelInfo(req, res) {
	logger.debug(
		'================ GET CHANNEL INFORMATION ======================');
	logger.debug('channelName : ' + req.params.channelName);
	let peer = req.query.peer;

	query.getChainInfo(peer, req.username, req.orgname).then(
		function(message) {
			return message;
		});
}
// Query to fetch all Installed/instantiated chaincodes
function QueryAllChaincodes(req, res) {
	var peer = req.query.peer;
	var installType = req.query.type;

	if (installType === 'installed') {
		logger.debug(
			'================ GET INSTALLED CHAINCODES ======================');
	} else {
		logger.debug(
			'================ GET INSTANTIATED CHAINCODES ======================');
	}

	query.getInstalledChaincodes(peer, installType, req.username, req.orgname)
	.then(function(message) {
		return message;
	});
}
// Query to fetch channels
function QueryFetchChannels(req, res) {
	logger.debug('================ GET CHANNELS ======================');
	logger.debug('peer: ' + req.query.peer);
	var peer = req.query.peer;
	if (!peer) {
		res.json(getErrorMessage('\'peer\''));
		return;
	}

	query.getChannels(peer, req.username, req.orgname)
	.then(function(
		message) {
		res.send(message);
	});
}

exports.defaultRun = defaultRun;
