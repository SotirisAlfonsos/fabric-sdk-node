'use strict';
var log4js = require('log4js');
var logger = log4js.getLogger('Smart contract install');
logger.setLevel('DEBUG');

var samples = require('./sampleRequests.json');
var install = require('./app/install-chaincode.js');


var defaultRun = function() {
        var org1ch1 = InstallCC(samples.allInOne_Org1_Ch1);
        var org2ch1 = InstallCC(samples.allInOne_Org2_Ch1);
        var org1ch2 = InstallCC(samples.allInOne_Org1_Ch2);
        var org2ch2 = InstallCC(samples.allInOne_Org2_Ch2);

        return Promise.all( [ org1ch1, org2ch1, org1ch2, org2ch2 ] ).then(message => {
            logger.debug('==========Smart contract installation is successful=========\n');
            return 'Smart contract installation is successful';
        }).catch((err) => {
            logger.debug('==========Smart contract installation failed=========');
            return 'Smart contract installation failed';
        });
}

// Install chaincode on target peers
function InstallCC(req) {
	logger.debug('==================== INSTALL CHAINCODE ==================');
	var peers = req.peers;
	var chaincodeName = req.chaincodeName;
	var chaincodePath = req.chaincodePath;
	var chaincodeVersion = req.chaincodeVersion;
	logger.debug('peers : ' + peers); // target peers list
	logger.debug('chaincodeName : ' + chaincodeName);
	logger.debug('chaincodePath  : ' + chaincodePath);
	logger.debug('chaincodeVersion  : ' + chaincodeVersion);
	logger.debug('channelName  : ' + req.channelName);
	if (!peers || peers.length == 0) {
		return 'Invalid peers';
	}
	if (!chaincodeName) {
		return 'Invalid chaincode name ';
	}
	if (!chaincodePath) {
		return 'Invalid chaincode path ';
	}
	if (!chaincodeVersion) {
		return 'Invalid chaincode version ';
	}

	return install.installChaincode(peers, chaincodeName, chaincodePath, chaincodeVersion, req.username, req.orgName, req.channelName)
	.then(function(message) {
		return message;
	}).catch((err) => {
	    logger.debug('========Install failure=======');
        throw err;
	});
}

exports.defaultRun = defaultRun;
