'use strict';
var log4js = require('log4js');
var logger = log4js.getLogger('Smart contract instantiate');
logger.setLevel('DEBUG');

var instantiate = require('./app/instantiate-chaincode.js');
var samples = require('./sampleRequests.json');

function defaultRun() {

    var org1ch1 = InstantiateCC(samples.allInOne_Org1_Ch1, samples.sampleInitFunction0);
    var org1ch2 = InstantiateCC(samples.allInOne_Org1_Ch2, samples.sampleInitFunction1);

    return Promise.all( [ org1ch1, org1ch2 ] ).then(message => {
        logger.info('==========Smart contract instantiation is successful=========\n');
        return 'Smart contract instantiation is successful';
    }).catch((err) => {
        logger.info('========Instantiate smart contract failed=======');
        return 'Instantiate smart contract failed';
    });

}

// Instantiate chaincode on target peers
function InstantiateCC(req, initArgs) {
	logger.debug('==================== INSTANTIATE CHAINCODE ==================');
	var chaincodeName = req.chaincodeName;
	var chaincodeVersion = req.chaincodeVersion;
	var channelName = req.channelName;
	var functionName = initArgs.functionName;
	var args = initArgs.args;
	logger.debug('channelName  : ' + channelName);
	logger.debug('chaincodeName : ' + chaincodeName);
	logger.debug('chaincodeVersion  : ' + chaincodeVersion);
	logger.debug('functionName  : ' + functionName);
	logger.debug('args  : ' + args);
	if (!chaincodeName) {
		return 'No chaincode name! ';
	}
	if (!chaincodeVersion) {
		return 'No chaincode version.';
	}
	if (!channelName) {
		return 'No channel Name.';
	}
	if (!functionName) {
		return 'No function Name.';
	}
	if (!args) {
		return 'No Args.';
	}

	return instantiate.instantiateChaincode(channelName, chaincodeName, chaincodeVersion, functionName, args, req.userName, req.orgName)
	.then(function(message) {
		return message;
	}).catch((err) => {
	    logger.debug('========Instantiate failure=======');
        throw err;
	});
}

exports.defaultRun = defaultRun;
