var log4js = require('log4js');
var logger = log4js.getLogger('Create channel');
logger.setLevel('DEBUG');

var channels = require('./app/create-channel.js');
var samples = require('./sampleRequests.json');

function defaultRun() {
  
    return CreateChannel(samples.allInOne_Org1_Ch1).then((message) => {
        return CreateChannel(samples.allInOne_Org2_Ch2).then( (message2) => {
            logger.debug('========Channel creation is successful=======\n');
            return 'Successful channel create';
        }).catch((err) => {
            logger.debug('========'+ samples.allInOne_Org2_Ch2.channelName + ' creation failed=======');
            return err;
        });
    }).catch((err) => {
        logger.debug('========'+ samples.allInOne_Org1_Ch1.channelName + ' creation failed=======');
        return err;
    });

  
}

// Create Channel
function CreateChannel(req) {
	logger.info('<<<<<<<<<<<<<<<<< C R E A T E  C H A N N E L >>>>>>>>>>>>>>>>>');
	logger.debug('Create new channel');
	var channelName = req.channelName;
	var channelConfigPath = req.channelConfigPath;
	logger.debug('Channel name : ' + channelName);
	logger.debug('channelConfigPath : ' + channelConfigPath); //../artifacts/channel/mychannel.tx
	if (!channelName) {
		return 'No channel name';
	}
	if (!channelConfigPath) {
		return 'No channel config path';
	}

	return channels.createChannel(channelName, channelConfigPath, req.userName, req.orgName).then( (message) => {
		return message;
	}).catch((err) => {
        logger.debug('========Create channel failure=======');
        throw err;
    });
}

exports.defaultRun = defaultRun;
