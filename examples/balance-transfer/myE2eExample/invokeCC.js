'use strict';
var log4js = require('log4js');
var logger = log4js.getLogger('Smart contract invoke');
logger.setLevel('DEBUG');

var invoke = require('./app/invoke-transaction.js');

var samples = require('./sampleRequests.json');


function defaultRun() {

    var results =[];


    /**********************************************************
    Smart contract containets exist for org1 in both channels, because of instantiate.
    Let the peers in org2 of both channels create their smart contract containers through this first invoke.
    This takes time, and if we start storming the net with transactions they will all be invalidates
    with an error message ( Chaincode is already running )
    **********************************************************/
    return InvokeCC(samples.allInOne_Org2_Ch1, samples.sampleInvokeFunction0).then(() => {
        return InvokeCC(samples.allInOne_Org2_Ch2, samples.sampleInvokeFunction1).then((message2) => {
            logger.debug('========First invoke success=======');
            return message2;
        /*}).then(() => {

            results.push(InvokeCC(samples.allInOne_Org2_Ch1, samples.sampleInvokeFunction0));
            results.push(InvokeCC(samples.allInOne_Org2_Ch2, samples.sampleInvokeFunction1));
            results.push(InvokeCC(samples.allInOne_Org1_Ch2, samples.sampleInvokeFunction1));
            results.push(InvokeCC(samples.allInOne_Org1_Ch1, samples.sampleInvokeFunction0));

            return Promise.all(results).then(message => {
               logger.debug('==========Second round of invoke success=========');
               return 'Second round of invoke success';
            }).catch((err) => {
               logger.debug('========Second round of invoke failed=======');
               return err;
            });*/
        }).then(() =>{

            return InvokeCC(samples.allInOne_Org2_Ch1, samples.sampleInvokeFunction0).then( (message) => {

                return 'successful channel join';
            }).then( () => {

                return InvokeCC(samples.allInOne_Org1_Ch1, samples.sampleInvokeFunction0).then( (resp) => {

                    return resp;
                }).catch((err) => {
                    return err;
                });
            }).then( () => {
                return InvokeCC(samples.allInOne_Org2_Ch1, samples.sampleInvokeFunction0).then( (resp) => {

                    return resp;
                }).catch((err) => {
                    return err;
                });
            }).then( () => {
                return InvokeCC(samples.allInOne_Org1_Ch1, samples.sampleInvokeFunction0).then( (resp) => {

                    return resp;
                }).catch((err) => {
                    return err;
                });
            }).then( () => {
                return InvokeCC(samples.allInOne_Org2_Ch1, samples.sampleInvokeFunction0).then( (resp) => {

                    return resp;
                }).catch((err) => {
                    return err;
                });
            }).then( () => {
                return InvokeCC(samples.allInOne_Org1_Ch1, samples.sampleInvokeFunction0).then( (resp) => {

                    return resp;
                }).catch((err) => {
                    return err;
                });
            }).then( () => {
                return InvokeCC(samples.allInOne_Org2_Ch1, samples.sampleInvokeFunction0).then( (resp) => {

                    return resp;
                }).catch((err) => {
                    return err;
                });
            }).then( () => {
                return InvokeCC(samples.allInOne_Org1_Ch1, samples.sampleInvokeFunction0).then( (resp) => {

                    return resp;
                }).catch((err) => {
                    return err;
                });
            }).then( () => {
                return InvokeCC(samples.allInOne_Org2_Ch1, samples.sampleInvokeFunction0).then( (resp) => {

                    return resp;
                }).catch((err) => {
                    return err;
                });
            }).catch((err) => {
                return err;
            });
        }).catch((err) => {
            return err;
		});


	/*for (var i=0; i<200; i++) {
                results.push( InvokeCC(samples.allInOne_Org2_Ch1, samples.sampleInvokeFunction0) );
                results.push( InvokeCC(samples.allInOne_Org2_Ch2, samples.sampleInvokeFunction1) );
            }
            return Promise.all(results).then(message => {
                logger.debug('==========Smart contract invocation storm is successful=========\n');
                return 'Smart contract invocation storm is successful';
	    }).catch((err) => {
               logger.debug('========Storm invoke failed=======');
               return err;
            });
        }).catch((err) => {
            logger.debug('========Invoke storm on smart contract failed=======');
            return err;
        });*/
    }).catch((err) => {
        logger.debug('========First invoke failed=======');
        return err;
    });

}

// Invoke transaction on chaincode on target peers
function InvokeCC(req, invokeArgs) {
	logger.debug('==================== INVOKE ON CHAINCODE ==================');
	var peers = invokeArgs.peers;
	var chaincodeName = req.chaincodeName;
	var channelName = req.channelName;
	var fcn = invokeArgs.fcn;
	var args = invokeArgs.args;
	logger.debug('channelName  : ' + channelName);
	logger.debug('chaincodeName : ' + chaincodeName);
	logger.debug('fcn  : ' + fcn);
	logger.debug('args  : ' + args);

	if (!peers || peers.length == 0) {
		return 'No peers';
	}

	if (!chaincodeName) {
		return 'No chaincode name';
	}

	if (!channelName) {
		return 'No channel name';
	}

	if (!fcn) {
		return 'No fcn';
	}

	if (!args) {
		return 'No args';
	}

	return invoke.invokeChaincode(peers, channelName, chaincodeName, fcn, args, req.userName, req.orgName)
	.then(function(message) {
		return message;
	}).catch((err) => {
	    logger.debug('========Invoke failure=======');
        throw err;
	});
}

exports.defaultRun = defaultRun;
