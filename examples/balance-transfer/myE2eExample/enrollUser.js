/**
 * Copyright 2017 IBM All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an 'AS IS' BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
'use strict';
var log4js = require('log4js');
var logger = log4js.getLogger('My application');
logger.setLevel('DEBUG');

var helper = require('./app/helper.js');
var samples = require('./sampleRequests.json')

function defaultRun() {

  helper.initHelper('../config.json', 'network-config.json');
  helper.initHelper('../config2.json', 'network-config2.json');
  setTimeout(waitFunc,5000);

}

// Register and enroll user. Wait for the initialisation.
function waitFunc() {

    RegisterUser(samples.allInOne_Org1_Ch1);
    RegisterUser(samples.allInOne_Org2_Ch1);
    RegisterUser(samples.allInOne_Org1_Ch2);
    RegisterUser(samples.allInOne_Org2_Ch2);

}

function RegisterUser(req) {
	var userName = req.userName;
	var orgName = req.orgName;
	logger.debug('Register and enroll user');
	logger.debug('User name : ' + userName);
	logger.debug('Org name  : ' + orgName);

	if (!userName) {
		return 'No user name';
	}
	if (!orgName) {
		return 'No org name';
	}

	return helper.getRegisteredUsers(userName, orgName, true, req.channelName).then( (response) => {

		if (response && typeof response !== 'string') {
			response.success = true;
			return response;
		}else {
			res = {
				success: false,
				message: response
			};
            return res;
		}
	});
}


exports.defaultRun = defaultRun;
