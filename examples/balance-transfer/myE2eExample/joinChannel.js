'use strict';

var log4js = require('log4js');
var logger = log4js.getLogger('Peers join channel');
logger.setLevel('DEBUG');

var join = require('./app/join-channel.js');
var samples = require('./sampleRequests.json');

function defaultRun() {
    return joinChannel(samples.allInOne_Org1_Ch1).then( (message) => {
        logger.debug('========Channel join is successful=======');
        return 'successful channel join';
    }).then( () => {
        return joinChannel(samples.allInOne_Org2_Ch1).then( (resp) => {
            return resp;
        }).catch((err) => {
            logger.debug('========Org2 failed to join channel 1=======');
            return err;
        });
    }).then( () => {
        return joinChannel(samples.allInOne_Org1_Ch2).then( (resp) => {
            return resp;
        }).catch((err) => {
            logger.debug('========Org1 failed to join channel 2=======');
            return err;
        });
    }).then( () => {
        return joinChannel(samples.allInOne_Org2_Ch2).then( (resp) => {
            return resp;
        }).catch((err) => {
            logger.debug('========Org2 failed to join channel 2=======');
            return err;
        });
    }).catch((err) => {
        logger.debug('========Org1 failed to join channel 1=======');
        return err;
    });
}

// Join Channel
function joinChannel(req) {

	logger.info('<<<<<<<<<<<<<<<<< J O I N  C H A N N E L >>>>>>>>>>>>>>>>>');
	var channelName = req.channelName;
	var peers = req.peers;
	logger.debug('channelName : ' + channelName);
	logger.debug('peers : ' + peers);

	if (!channelName) {
		return 'No channel name';
	}

	if (!peers || peers.length == 0) {
		return 'Peers not set';
	}

	return join.joinChannel(channelName, peers, req.userName, req.orgName).then((message) => {
		return message;
	}).catch((err) => {
	    logger.debug(err + '\n' +err.stack);
	    logger.debug('========Join failure=======');
        throw err;
	});
}

exports.defaultRun = defaultRun;
