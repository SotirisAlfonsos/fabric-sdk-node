'use strict';
var log4js = require('log4js');
var logger = log4js.getLogger('My application, Head');
logger.setLevel('DEBUG');

var enroll = require('./enrollUser.js');
enroll.defaultRun();
setTimeout(createChannel, 10000);

function createChannel() {
    var createCh = require('./createChannel.js');
    createCh.defaultRun().then(function(message) {
        setTimeout(joinChannel,10000);
    });
}


function joinChannel() {
    var joinCh = require('./joinChannel.js');
    joinCh.defaultRun().then(function(message) {
        installChaincode();
    });
}

function installChaincode() {
    var install = require('./installCC.js');
    install.defaultRun().then(function(message) {
        instantiateChaincode();
    });
}

function instantiateChaincode() {
    var instantiate = require('./instantiateCC.js');
    instantiate.defaultRun().then(function(message) {
        addNetworkDelay();
    });
}

function addNetworkDelay() {
//    var child = require('child_process');
//    child.execSync('./../scripts/networkDel.sh up');
	invokeChaincode();
}

function invokeChaincode() {
    var invoke = require('./invokeCC.js');
    var i;
    var secondStep=[];
    invoke.defaultRun().then(function(message) {
        var func = require('./app/invoke-transaction.js')
        var firstStep = func.FirstParttimes;
        var secondSteps = func.SecondParttimes;
        for (i=0; i<secondSteps.length; i+=2) {
            if (secondSteps[i]>=secondSteps[i+1]) secondStep.push(secondSteps[i]);
            else secondStep.push(secondSteps[i+1]);
        }
        for (i=0; i<firstStep.length; i++) {
            logger.info("Tx "+i+" form client to peers and back to the client execution time "+ firstStep[i] + "\n" + " from client to orderer to peers execution time " + secondStep[i]);
        }
        removeNetworkDelay();
    });
}

function removeNetworkDelay() {
//    var child = require('child_process');
//    child.execSync('./../scripts/networkDel.sh down');

	queryChannelStates();
}

function queryChannelStates() {
    var query = require('./queryCC.js');
    query.defaultRun();
}

