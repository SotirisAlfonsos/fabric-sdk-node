/**
 * Copyright 2017 IBM All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an 'AS IS' BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
'use strict';
var log4js = require('log4js');
var logger = log4js.getLogger('Helper');
logger.setLevel('INFO');

var path = require('path');
var util = require('util');
var fs = require('fs-extra');
var User = require('fabric-client/lib/User.js');
var crypto = require('crypto');
var copService = require('fabric-ca-client');

var channelsGlobal = {};

var initHelper = function(configPath, networkFile) {

    var clients = {};
    var channels = {};
    var caClients = {};
    var config = require(configPath);

    var hfc = require('fabric-client');
    hfc.addConfigFile(path.join(__dirname, networkFile));
    hfc.setLogger(logger);
    var networkName = networkFile.split('.');
    var ORGS = hfc.getConfigSetting(networkName[0]);

    channelsGlobal[config.channelName] = {
        hfc: hfc,
        config: config,
        ORGS: ORGS,
        clients: clients,
        channels: channels,
        caClients: caClients
    };

    for (let key in ORGS) {
    	if (key.indexOf('org') === 0) {
    		let client = new hfc();

    		let cryptoSuite = hfc.newCryptoSuite();
    		cryptoSuite.setCryptoKeyStore(hfc.newCryptoKeyStore({path: getKeyStoreForOrg(ORGS[key].name, config.channelName)}));
    		client.setCryptoSuite(cryptoSuite);

    		let channel = client.newChannel(config.channelName);
    		channel.addOrderer(newOrderer(client, config.channelName));

    		clients[key] = client;
    		channels[key] = channel;
    		logger.debug('Channels ' + key + ' ' + channels[key]);
    		setupPeers(channel, key, client, config.channelName);

    		let caUrl = ORGS[key].ca;

    		caClients[key] = new copService(caUrl, null /*defautl TLS opts*/, '' /* default CA */, cryptoSuite);
    	}
    }
    channelsGlobal[config.channelName].clients = clients;
    channelsGlobal[config.channelName].channels = channels;
    channelsGlobal[config.channelName].caClients = caClients;

}

function setupPeers(channel, org, client, channelName) {
	for (let key in channelsGlobal[channelName].ORGS[org]) {
		if (key.indexOf('peer') === 0) {
			let data = fs.readFileSync(path.join(__dirname, channelsGlobal[channelName].ORGS[org][key]['tls_cacerts']));
			let peer = client.newPeer(
				channelsGlobal[channelName].ORGS[org][key].requests,
				{
					pem: Buffer.from(data).toString(),
					'ssl-target-name-override': channelsGlobal[channelName].ORGS[org][key]['server-hostname']
				}
			);

			channel.addPeer(peer);
		}
	}
}

function newOrderer(client, channelName) {
	var caRootsPath = channelsGlobal[channelName].ORGS.orderer.tls_cacerts;
	let data = fs.readFileSync(path.join(__dirname, caRootsPath));
	let caroots = Buffer.from(data).toString();
	return client.newOrderer(channelsGlobal[channelName].config.orderer, {
		'pem': caroots,
		'ssl-target-name-override': channelsGlobal[channelName].ORGS.orderer['server-hostname']
	});
}

function readAllFiles(dir) {
	var files = fs.readdirSync(dir);
	var certs = [];
	files.forEach((file_name) => {
		let file_path = path.join(dir,file_name);
		let data = fs.readFileSync(file_path);
		certs.push(data);
	});
	return certs;
}

function getOrgName(org, channelName) {
	return channelsGlobal[channelName].ORGS[org].name;
}

function getKeyStoreForOrg(org, channelName) {
	return channelsGlobal[channelName].config.keyValueStore + '_' + org;
}

function newRemotes(urls, channelName, forPeers, userOrg) {
	var targets = [];
	// find the peer that match the urls
	var ORGS = getORGS(channelName);
	outer:
	for (let index in urls) {
		let peerUrl = urls[index];

		let found = false;
		for (let key in ORGS) {
			if (key.indexOf('org') === 0) {
				// if looking for event hubs, an app can only connect to
				// event hubs in its own org
				if (!forPeers && key !== userOrg) {
					continue;
				}

				let org = ORGS[key];
				let client = getClientForOrg(key, channelName);

				for (let prop in org) {
					if (prop.indexOf('peer') === 0) {
						if (org[prop]['requests'].indexOf(peerUrl) >= 0) {
							// found a peer matching the subject url
							if (forPeers) {
								let data = fs.readFileSync(path.join(__dirname, org[prop]['tls_cacerts']));
								targets.push(client.newPeer('grpcs://' + peerUrl, {
									pem: Buffer.from(data).toString(),
									'ssl-target-name-override': org[prop]['server-hostname']
								}));

								continue outer;
							} else {
								let eh = client.newEventHub();
								let data = fs.readFileSync(path.join(__dirname, org[prop]['tls_cacerts']));
								eh.setPeerAddr(org[prop]['events'], {
									pem: Buffer.from(data).toString(),
									'ssl-target-name-override': org[prop]['server-hostname']
								});
								targets.push(eh);

								continue outer;
							}
						}
					}
				}
			}
		}

		if (!found) {
			logger.error(util.format('Failed to find a peer matching the url %s', peerUrl));
		}
	}
    logger.info('Sending to peers ' + targets);
	return targets;
}

var getChannelForOrg = function(org, channelName) {
	return channelsGlobal[channelName].channels[org];
};

var getClientForOrg = function(org, channelName) {
	return channelsGlobal[channelName].clients[org];
};

var getORGS = function(channelName) {
	return channelsGlobal[channelName].ORGS;
};

var newPeers = function(urls, channelName) {
	return newRemotes(urls, channelName, true);
};

var newEventHubs = function(urls, org, channelName) {
	return newRemotes(urls, channelName, false, org);
};

var getMspID = function(org, channelName) {
	logger.debug('Msp ID : ' + channelsGlobal[channelName].ORGS[org].mspid);
	return channelsGlobal[channelName].ORGS[org].mspid;
};

var getAdminUser = function(userOrg, channelName) {
	var users = channelsGlobal[channelName].config.users;
	var username = users[0].username;
	var password = users[0].secret;
	var member;
	var hfc = channelsGlobal[channelName].hfc;
	var client = getClientForOrg(userOrg, channelName);

	return hfc.newDefaultKeyValueStore({
		path: getKeyStoreForOrg(getOrgName(userOrg, channelName), channelName)
	}).then((store) => {
		client.setStateStore(store);
		// clearing the user context before switching
		client._userContext = null;
		return client.getUserContext(username, true).then((user) => {
			if (user && user.isEnrolled()) {
				logger.debug('Successfully loaded member from persistence');
				return user;
			} else {
				let caClient = channelsGlobal[channelName].caClients[userOrg];
				// need to enroll it with CA server

				return caClient.enroll({
					enrollmentID: username,
					enrollmentSecret: password
				}).then((enrollment) => {
					logger.debug('Successfully enrolled user \'' + username + '\'');
					member = new User(username);
					member.setCryptoSuite(client.getCryptoSuite());
					return member.setEnrollment(enrollment.key, enrollment.certificate, getMspID(userOrg, channelName));
				}).then(() => {
					return client.setUserContext(member);
				}).then(() => {
					return member;
				}).catch((err) => {
					logger.error('Failed to enroll and persist user. Error: ' + err.stack ?
					    err.stack : err);
					return null;
				});
			}
		});
	});
};

var getRegisteredUsers = function(username, userOrg, isJson, channelName) {
	var member;
	var hfc = channelsGlobal[channelName].hfc;
	var client = getClientForOrg(userOrg, channelName);
	var enrollmentSecret = null;
	return hfc.newDefaultKeyValueStore({
		path: getKeyStoreForOrg(getOrgName(userOrg, channelName), channelName)
	}).then((store) => {
		client.setStateStore(store);
		// clearing the user context before switching
		client._userContext = null;
		return client.getUserContext(username, true).then((user) => {
			if (user && user.isEnrolled()) {
				logger.info('Successfully loaded member from persistence');
				return user;
			} else {
				let caClient = channelsGlobal[channelName].caClients[userOrg];
				return getAdminUser(userOrg, channelName).then(function(adminUserObj) {
					member = adminUserObj;
					var temp = channelName.split('l');
					if (!temp[1]) temp[1]="1";

					return caClient.register({
						enrollmentID: username,
						affiliation: userOrg + '.department' + temp[1]
					}, member);
				}).then((secret) => {
					enrollmentSecret = secret;
					logger.debug(username + ' registered successfully');
					return caClient.enroll({
						enrollmentID: username,
						enrollmentSecret: secret
					});
				}, (err) => {
					logger.debug(username + ' failed to register');
					return '' + err;

				}).then((message) => {
					if (message && typeof message === 'string' && message.includes(
							'Error:')) {
						logger.error(username + ' enrollment failed');
						return message;
					}
					logger.debug(username + ' enrolled successfully');

					member = new User(username);
					member._enrollmentSecret = enrollmentSecret;
					return member.setEnrollment(message.key, message.certificate, getMspID(userOrg, channelName));
				}).then(() => {
					client.setUserContext(member);
					return member;
				}, (err) => {
					logger.error(util.format('%s enroll failed: %s', username, err.stack ? err.stack : err));
					return '' + err;
				});
			}
		});
	}).then((user) => {
		if (isJson && isJson === true) {
			var response = {
				success: true,
				secret: user._enrollmentSecret,
				message: username + ' enrolled Successfully',
			};
			return response;
		}
		return user;
	}, (err) => {
		logger.error(util.format('Failed to get registered user: %s, error: %s', username, err.stack ? err.stack : err));
		return '' + err;
	});
};

var getOrgAdmin = function(userOrg, channelName) {
	var admin = channelsGlobal[channelName].ORGS[userOrg].admin;
	logger.debug(admin);
	var keyPath = path.join(__dirname, util.format('../../artifacts/crypto-config/peerOrganizations/%s.example.com/users/Admin@%s.example.com/msp/keystore', userOrg, userOrg));
	var keyPEM = Buffer.from(readAllFiles(keyPath)[0]).toString();
	var certPath = path.join(__dirname, util.format('../../artifacts/crypto-config/peerOrganizations/%s.example.com/users/Admin@%s.example.com/msp/signcerts', userOrg, userOrg));
	var certPEM = readAllFiles(certPath)[0].toString();
    var hfc = channelsGlobal[channelName].hfc;

	var client = getClientForOrg(userOrg, channelName);
	var cryptoSuite = hfc.newCryptoSuite();
	if (userOrg) {
		cryptoSuite.setCryptoKeyStore(hfc.newCryptoKeyStore({path: getKeyStoreForOrg(getOrgName(userOrg, channelName), channelName)}));
		client.setCryptoSuite(cryptoSuite);
	}

	return hfc.newDefaultKeyValueStore({
		path: getKeyStoreForOrg(getOrgName(userOrg, channelName),channelName)
	}).then((store) => {
		client.setStateStore(store);

		return client.createUser({
			username: 'peer'+userOrg+'Admin',
			mspid: getMspID(userOrg, channelName),
			cryptoContent: {
				privateKeyPEM: keyPEM,
				signedCertPEM: certPEM
			}
		});
	});
};

var setupChaincodeDeploy = function(channelName) {
    logger.debug(channelName);
	process.env.GOPATH = path.join(__dirname, channelsGlobal[channelName].config.GOPATH);
};

var getLogger = function(moduleName) {
	var logger = log4js.getLogger(moduleName);
	logger.setLevel('DEBUG');
	return logger;
};

var getPeerAddressByName = function(org, peer, channelName) {
	var address = channelsGlobal[channelName].ORGS[org][peer].requests;
	return address.split('grpcs://')[1];
};

exports.getChannelForOrg = getChannelForOrg;
exports.getClientForOrg = getClientForOrg;
exports.getLogger = getLogger;
exports.setupChaincodeDeploy = setupChaincodeDeploy;
exports.getMspID = getMspID;
exports.getORGS = getORGS;
exports.newPeers = newPeers;
exports.newEventHubs = newEventHubs;
exports.getPeerAddressByName = getPeerAddressByName;
exports.getRegisteredUsers = getRegisteredUsers;
exports.getOrgAdmin = getOrgAdmin;
exports.initHelper = initHelper;